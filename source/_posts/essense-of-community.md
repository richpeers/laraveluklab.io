---
extends: _layouts.post
title: The Essense of Community!
author: Steve Popoola
date: 2017-05-01
tag: General
section: body
---

If there is one thing that attracts many people to Laravel, it's more about the community than the framework itself. Laravel UK is one
of such community.

It is a place where you have people with varying degrees of expertise. Unlike some communities where some people are
ridiculed when they ask some questions, community members are always willing and ready to help and challenge each other.

Although the community was created for Laravel developers in the UK, we have others joining us from other countries and they have been welcomed with open arms.

This is what a community should look like and I am proud to be an active part of this friendly bunch!
