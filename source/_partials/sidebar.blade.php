<div class="widget widget_search">
    <h3>Search</h3>
    <form action="#">

        <input type="text" value="Search here..."
               onblur="if(this.value == '') { this.value = 'Search here...'; }"
               onfocus="if (this.value == 'Search here...') { this.value = ''; }" class="text-search">
        <input type="submit" value="" class="submit-search">

    </form>
</div>

<div class="widget widget_categories group">
    <h3>Categories.</h3>
    <ul>
        <li><a href="#" title="">Laravel</a> (2)</li>
        <li><a href="#" title="">MySQL</a> (0)</li>
        <li><a href="#" title="">Deployment</a> (0)</li>
        <li><a href="#" title="">Jobs</a> (0)</li>
        <li><a href="#" title="">Resources</a> (0)</li>
        <li><a href="#" title="">Events</a> (0)</li>
    </ul>
</div>


<div class="widget widget_tags">
    <h3>Post Tags.</h3>

    <div class="tagcloud group">
        <a href="#">Laravel</a>
        <a href="#">PHP</a>
        <a href="#">MySQL</a>
        <a href="#">Valet</a>
        <a href="#">Docker</a>
        <a href="#">Unit Tests</a>
        <a href="#">Continous Integration</a>
    </div>

</div>

<div class="widget widget_popular">
    <h3>Recent Posts.</h3>


    <ul class="link-list">
        @foreach($posts as $post)
            <li><a href="#">{{ $post->title }}</a></li>
        @endforeach
    </ul>
</div>
