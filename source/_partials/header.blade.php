<div class="row">
  <div class="header-content twelve columns">
    <h1 id="logo-text">
    <img src="{{ $page->baseUrl }}/images/laraveluk.png" class="logo" alt="Laravel UK">
    <a href="{{ $page->baseUrl }}/" title="">Laravel UK</a></h1>
    <p id="intro">The most exciting coding group in the UK</p>
  </div>
</div>

<nav id="nav-wrap">
  <a class="mobile-btn" href="#nav-wrap" title="Show navigation">Show Menu</a>

  <a class="mobile-btn" href="#" title="Hide navigation">Hide Menu</a>

  <div class="row">

    <ul id="nav" class="nav">
      <li class="{{ $page->getPath() =='' ? 'current': '' }}"><a href="{{ $page->baseUrl }}/">Home</a></li>
      <li class="{{ $page->selected('about') }}"><a href="{{ $page->baseUrl }}/about">Who We Are</a></li>
      <li class="{{ $page->selected('archives') }}"><a href="{{ $page->baseUrl }}/archives">Archives</a></li>
      <li class="{{ $page->selected('resources') }}"><a href="{{ $page->baseUrl }}/resources">Resources</a></li>
    </ul> <!-- end #nav -->
    </div>

</nav> <!-- end #nav-wrap -->
